import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;

/**
 * Created by Mykola_Fedechko on 5/24/2017.
 */
public class BaseTest {


    protected WebDriver driver;

    @BeforeClass
//    @Parameters("browser")
    public void setup() {
        System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

//        DesiredCapabilities capability = null;
//        if (browser.equals("firefox")) {
//            capability = DesiredCapabilities.firefox();
//        } else if (browser.equals("chrome")) {
//            capability = DesiredCapabilities.chrome();
//        }

//        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("http://dmsu.gov.ua/online");
    }

//    @AfterClass
//    public void tearDown() {
//        driver.quit();
//    }
}
