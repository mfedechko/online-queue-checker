import org.testng.annotations.Test;
import page.OnlineQueuePage;

/**
 * Created by Mykola_Fedechko on 5/24/2017.
 */
public class OnlineQueueTest extends BaseTest {

    @Test
    public void checkDates() {

        for (int i = 0; i < 10; i++) {
            OnlineQueuePage page = new OnlineQueuePage(driver);
            page.switchToOnlineQueueForm();
            page.selectRegion("4600000000");
            page.selectOrganization("110");
            page.selectService("type_passport");
            page.checkAvailableDates();

            driver.navigate().refresh();
        }
    }
}
