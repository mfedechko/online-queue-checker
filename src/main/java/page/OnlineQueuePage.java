package page;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.exceptions.ElementShouldBeEnabledException;
import net.thucydides.core.webdriver.exceptions.ElementShouldBePresentException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Mykola_Fedechko on 5/22/2017.
 */
public class OnlineQueuePage extends PageObject {

    private static final Logger LOG = LoggerFactory.getLogger(OnlineQueuePage.class);

    @FindBy(xpath = "//div[@id='articlepxfontsize1']/div/iframe")
    private WebElementFacade queueFormFrame;

    @FindBy(id = "step0")
    private WebElementFacade step1;

    @FindBy(id = "selected_region")
    private WebElementFacade regionsDropDown;

    @FindBy(id = "regions")
    private WebElementFacade regions;

    @FindBy(id = "selected_org")
    private WebElementFacade orgDropDown;

    @FindBy(id = "orgs")
    private WebElementFacade orgs;

    @FindBy(id = "servs")
    private WebElementFacade servs;

    @FindBy(id = "calendar")
    private WebElementFacade calendar;

    private WebElementFacade service;

    private String regionCode;
    private String organization;

    public OnlineQueuePage(final WebDriver driver) {
        super(driver);
    }

    public void switchToOnlineQueueForm() {
        getDriver().switchTo().frame(queueFormFrame);

    }

    public void selectRegion(final String regionCode) {
        try {
            this.regionCode = regionCode;
            regionsDropDown.waitUntilClickable();
            regionsDropDown.click();
            final WebElementFacade region = regions.find(By.xpath(String.format("//li[@data-id='%s']", regionCode)));
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", region);
            region.waitUntilClickable();
            region.click();
        } catch (WebDriverException e) {
            handleNotVisibleRegion(regionCode);

        }
    }

    public void selectOrganization(final String orgCode) {

        try {
            organization = orgCode;
            orgDropDown.waitUntilClickable();
            orgDropDown.click();
            final WebElementFacade org = orgs.find(By.xpath(String.format("//li[@data-id='%s']", orgCode)));
            org.waitUntilClickable();
            org.click();
        } catch (ElementShouldBeEnabledException | NoSuchElementException e) {
            handleNotVisibleOrganization(orgCode);
        }
    }

    public void selectService(final String serviceType) {
        try {
            servs.waitUntilClickable();
            service = servs.find(By.xpath(String.format("./div[@data-type='%s']/div", serviceType)));
            service.waitUntilClickable();
            service.click();
        } catch (ElementShouldBeEnabledException e) {
            handleNotVisibleService(service);
        }
    }

    public void checkAvailableDates() {
        try {
            calendar.waitUntilPresent();
            List<WebElementFacade> dates = calendar.thenFindAll(By.xpath("//li"));
            List<WebElementFacade> availableDates = dates.stream()
                    .filter(date -> date.getAttribute("class").equals("status-0"))
                    .collect(Collectors.toList());
            System.out.println(availableDates.stream().map(ad -> ad.getAttribute("data-id")).collect(Collectors.toList()));
        } catch (ElementShouldBePresentException exception) {
            handleNotVisibleService(service);

        }
    }

    private void handleNotVisibleRegion(final String regionCode) {
        System.out.println("Regions list is not available. Page will be refreshed...");
        getDriver().navigate().refresh();
        getDriver().switchTo().frame(queueFormFrame);
        selectRegion(regionCode);
    }

    private void handleNotVisibleOrganization(String orgCode) {
        System.out.println("Organizations list is not available. Region will be selected again...");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", regionsDropDown);
        selectRegion(regionCode);
        selectOrganization(orgCode);
    }

    private void handleNotVisibleService(WebElementFacade service) {
        System.out.println("Service is not available. Organization will be selected again...");
        try {
            selectOrganization(organization);
            service.waitUntilClickable();
            service.click();
        } catch (Exception e) {
            getDriver().navigate().refresh();
        }

    }


}
